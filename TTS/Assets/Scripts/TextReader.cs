using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class TextReader : MonoBehaviour
{
    public InputField inputField;
    public AudioSource aSource;

    Dictionary<string, AudioSample> sampleDictionary;

    List<AudioSample> readingList;
    bool previousWasPause;

    public Toggle toggleH;
    public Toggle toggleR;

    public Slider volumeSlider;
    public Slider pitchSlider;

    bool stopReading;

    Coroutine readText;

    void Start()
    {
        InitializeDictionary();
    }

    public void InitializeDictionary()
    {
        sampleDictionary = new Dictionary<string, AudioSample>();
        // get all audio samples from resources
        Object[] allSamples = Resources.LoadAll("", typeof(AudioSample));

        // add all samples to sampleDictionary
        for (int i = 0; i < allSamples.Length; i++)
        {
            AudioSample newSample = (AudioSample)allSamples[i]; // Turn Objects into AudioSamples
            sampleDictionary.Add(newSample.name.ToLower(), newSample);
        }

        // pauses are names "_", "_2", "_3", replace them for punctuation characters
        if (sampleDictionary.ContainsKey("_"))
        {
            sampleDictionary.Add(" ", sampleDictionary["_"]);
            sampleDictionary.Add(",", sampleDictionary["_"]);
            sampleDictionary.Add(";", sampleDictionary["_"]);
            sampleDictionary.Remove("_");
        }
        if (sampleDictionary.ContainsKey("_2"))
        {
            sampleDictionary.Add(".", sampleDictionary["_2"]);
            sampleDictionary.Add(":", sampleDictionary["_2"]);
            sampleDictionary.Remove("_2");
        }
        if (sampleDictionary.ContainsKey("_3"))
        {
            sampleDictionary.Add("\n", sampleDictionary["_3"]);
            sampleDictionary.Remove("_3");
        }

        // add keys that don't have their own sample since they use duplicated sounds
        if (sampleDictionary.ContainsKey("ka"))
        {
            sampleDictionary.Add("ca", sampleDictionary["ka"]);
        }
        if (sampleDictionary.ContainsKey("ze"))
        {
            sampleDictionary.Add("ce", sampleDictionary["ze"]);
        }
        if (sampleDictionary.ContainsKey("zi"))
        {
            sampleDictionary.Add("ci", sampleDictionary["zi"]);
        }
        if (sampleDictionary.ContainsKey("ko"))
        {
            sampleDictionary.Add("co", sampleDictionary["ko"]);
        }
        if (sampleDictionary.ContainsKey("ku"))
        {
            sampleDictionary.Add("cu", sampleDictionary["ku"]);
        }

        if (sampleDictionary.ContainsKey("k"))
        {
            sampleDictionary.Add("q", sampleDictionary["k"]);
        }
        if (sampleDictionary.ContainsKey("ke"))
        {
            sampleDictionary.Add("que", sampleDictionary["ke"]);
        }
        if (sampleDictionary.ContainsKey("ki"))
        {
            sampleDictionary.Add("qui", sampleDictionary["ki"]);
        }
        if (sampleDictionary.ContainsKey("ku"))
        {
            sampleDictionary.Add("qu", sampleDictionary["ku"]);
        }

        if (sampleDictionary.ContainsKey("ba"))
        {
            sampleDictionary.Add("va", sampleDictionary["ba"]);
        }
        if (sampleDictionary.ContainsKey("be"))
        {
            sampleDictionary.Add("ve", sampleDictionary["be"]);
        }
        if (sampleDictionary.ContainsKey("bi"))
        {
            sampleDictionary.Add("vi", sampleDictionary["bi"]);
        }
        if (sampleDictionary.ContainsKey("bo"))
        {
            sampleDictionary.Add("vo", sampleDictionary["bo"]);
        }
        if (sampleDictionary.ContainsKey("bu"))
        {
            sampleDictionary.Add("vu", sampleDictionary["bu"]);
        }

        if (sampleDictionary.ContainsKey("ya"))
        {
            sampleDictionary.Add("lla", sampleDictionary["ya"]);
        }
        if (sampleDictionary.ContainsKey("ye"))
        {
            sampleDictionary.Add("lle", sampleDictionary["ye"]);
        }
        if (sampleDictionary.ContainsKey("yi"))
        {
            sampleDictionary.Add("lli", sampleDictionary["yi"]);
        }
        if (sampleDictionary.ContainsKey("yo"))
        {
            sampleDictionary.Add("llo", sampleDictionary["yo"]);
        }
        if (sampleDictionary.ContainsKey("yu"))
        {
            sampleDictionary.Add("llu", sampleDictionary["yu"]);
        }

        // if toggleH, make h sounds silent
        if (toggleH.isOn)
        {
            if (sampleDictionary.ContainsKey("ha"))
            {
                sampleDictionary["ha"] = sampleDictionary["a"];
            }
            if (sampleDictionary.ContainsKey("he"))
            {
                sampleDictionary["he"] = sampleDictionary["e"];
            }
            if (sampleDictionary.ContainsKey("hi"))
            {
                sampleDictionary["hi"] = sampleDictionary["i"];
            }
            if (sampleDictionary.ContainsKey("ho"))
            {
                sampleDictionary["ho"] = sampleDictionary["o"];
            }
            if (sampleDictionary.ContainsKey("hu"))
            {
                sampleDictionary["hu"] = sampleDictionary["u"];
            }
        }
    }

    public void ManageText() // called to start the reading
    {
        stopReading = false;
        ParseText();

        if(readText != null)
        {
            StopCoroutine(readText);
        }
        readText = StartCoroutine(ReadText());
    }

    void ParseText() // turn the input text into a queue of sound samples
    {
        readingList = new List<AudioSample>();
        string text = inputField.text.ToLower();
        text = RemoveDiacritics(text);
        string textUnit = "";

        // replace numbers for their pronuntiation
        text = text.Replace("0", "cero");
        text = text.Replace("1", "uno");
        text = text.Replace("2", "dos");
        text = text.Replace("3", "tres");
        text = text.Replace("4", "cuatro");
        text = text.Replace("5", "cinco");
        text = text.Replace("6", "seis");
        text = text.Replace("7", "siete");
        text = text.Replace("8", "ocho");
        text = text.Replace("9", "nueve");

        // go through each character of the input text
        for (int i = 0; i < text.Length; i ++)
        {
            // check combinations of 3 characters
            if (text.Length >= i + 3)
            {
                textUnit = text.Substring(i, 3);
                if (SearchList(textUnit)) // if that combination is inside the dictionary, move the index and continue
                {
                    i += 2;
                    continue;
                }
            }

            // check combinations of 2 characters
            if (text.Length >= i + 2)
            {
                textUnit = text.Substring(i, 2);
                if (SearchList(textUnit)) // if that combination is inside the dictionary, move the index and continue
                {
                    i++;
                    continue;
                }
            }

            // check a single character
            textUnit = text.Substring(i, 1);
            SearchList(textUnit);
        }
    }

    bool SearchList(string textUnit) // search a key in the samples dictionary
    {
        if (sampleDictionary.ContainsKey(textUnit))
        {
            // add the sample to the queue to be read
            readingList.Add(sampleDictionary[textUnit]);

            // make adjustemts if initial R is set to hard
            if (toggleR.isOn)
            {
                if (readingList.Count == 1) // if it's the first sample of the whole text, change it, since it must be the first consonant of a word
                {
                    if (textUnit == "ra")
                    {
                        readingList.RemoveAt(readingList.Count - 1);
                        readingList.Add(sampleDictionary["rra"]);
                    }
                    else if (textUnit == "re")
                    {
                        readingList.RemoveAt(readingList.Count - 1);
                        readingList.Add(sampleDictionary["rre"]);
                    }
                    else if (textUnit == "ri")
                    {
                        readingList.RemoveAt(readingList.Count - 1);
                        readingList.Add(sampleDictionary["rri"]);
                    }
                    else if (textUnit == "ro")
                    {
                        readingList.RemoveAt(readingList.Count - 1);
                        readingList.Add(sampleDictionary["rro"]);
                    }
                    else if (textUnit == "ru")
                    {
                        readingList.RemoveAt(readingList.Count - 1);
                        readingList.Add(sampleDictionary["rru"]);
                    }
                }
                else if(readingList.Count > 1) // if it's not the first character of the whole text
                {
                    if (readingList[readingList.Count - 2] == sampleDictionary[" "]) // if the previous sample was a space, so this is the first consonant of a word
                    {
                        if (textUnit == "ra")
                        {
                            readingList.RemoveAt(readingList.Count - 1); // remove the original ra sample
                            readingList.Add(sampleDictionary["rra"]);
                        }
                        else if (textUnit == "re")
                        {
                            readingList.RemoveAt(readingList.Count - 1);
                            readingList.Add(sampleDictionary["rre"]);
                        }
                        else if (textUnit == "ri")
                        {
                            readingList.RemoveAt(readingList.Count - 1);
                            readingList.Add(sampleDictionary["rri"]);
                        }
                        else if (textUnit == "ro")
                        {
                            readingList.RemoveAt(readingList.Count - 1);
                            readingList.Add(sampleDictionary["rro"]);
                        }
                        else if (textUnit == "ru")
                        {
                            readingList.RemoveAt(readingList.Count - 1);
                            readingList.Add(sampleDictionary["rru"]);
                        }
                    }
                }
            }

            return true;
        }

        return false;
    }

    public void StopReading()
    {
        stopReading = true;
    }

    IEnumerator ReadText()
    {
        for (int i = 0; i < readingList.Count; i++) // while there are samples to read
        {
            if (stopReading)
            {
                stopReading = false;
                yield break;
            }

            if(readingList[i].clip != null)
            {
                // apply pitch
                float pitch = pitchSlider.value/10f;
                aSource.outputAudioMixerGroup.audioMixer.SetFloat("PitchShift", pitch);

                // apply volume
                float vol = volumeSlider.value / 100f;
                vol = vol * vol;

                // play the sample
                aSource.PlayOneShot(readingList[i].clip, vol);
            }

            // check if the previous sound was a pause, to know if preChain time should be ignore so the pause is played in full
            if (readingList[i].name == "_" || readingList[i].name == "_1" || readingList[i].name == "_2" || readingList[i].name == "_3")
            {
                previousWasPause = true;
            }
            else
            {
                previousWasPause = false;
            }

            // get the chainTime
            float chainTime = readingList[i].chainTime;
            // if it's not the last sample and it's not a pause, substract the preChainTime of the next sample
            if (i < readingList.Count - 1 && !previousWasPause)
            {
                chainTime -= readingList[i + 1].preChainTime;
            }
            chainTime = Mathf.Clamp(chainTime, 0f, 1f);

            // waut for the sample to be read
            yield return new WaitForSeconds(chainTime);
        }
    }

    static string RemoveDiacritics(string text) 
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in text)
        {
            if(c == 'á')
            {
                sb.Append('a');
            }
            else if (c == 'é')
            {
                sb.Append('e');
            }
            else if (c == 'í')
            {
                sb.Append('i');
            }
            else if (c == 'ó')
            {
                sb.Append('o');
            }
            else if (c == 'ú')
            {
                sb.Append('u');
            }
            else
            {
                sb.Append(c);
            }
        }

        return sb.ToString();
    }
}
