using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeLabel : MonoBehaviour
{
    public Slider slider;
    public Text text;

    public void UpdateLabel()
    {
        text.text = slider.value + "";
    }
}
