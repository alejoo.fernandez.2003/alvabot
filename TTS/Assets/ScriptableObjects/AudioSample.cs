using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioSample", menuName = "Audio Sample")]
public class AudioSample : ScriptableObject
{
    public AudioClip clip;
    // time at witch the next sound should start
    [Range(0.0f, 0.5f)]
    public float chainTime = 0.2f;
    // time that should be overlaped with the previous sound
    [Range(0.0f, 0.5f)]
    public float preChainTime = 0f;
}